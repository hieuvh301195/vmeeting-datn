import React, { Component, useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    Image
} from 'react-native'
import { COLOR } from '../constant/Colors'
const { width, height } = Dimensions.get('window')

export default function Avatar(props) {
    return (
        <View>
            <View style={styles.container}>
                <Image 
                    style={styles.avatar}
                     source={require('../Image/Aerith.jpg')}
                />
                <Text style={styles.avaName}>Nguyễn Anh Tú</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
       width:width,
       height:height/3,
       alignItems:'center'
    },
    avatar:{
        width:width/3,
        height:height/5,
        borderRadius:999,
        marginTop:26,
    },
    avaName:{
        fontFamily:'Sanchez',
        fontStyle:'normal',
        fontWeight:'normal',
        fontSize:16,
        lineHeight:20,
        color:COLOR.AVATAR_NAME,
        textTransform:'uppercase',
        paddingVertical:12
    }
})