import React, { Component, useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    StatusBar,
    FlatList,
    TouchableOpacity
} from 'react-native'
const { width, height } = Dimensions.get('window')
import { getString } from '../constant/String'
import { COLOR } from '../constant/Colors'
import Icon from 'react-native-vector-icons/FontAwesome5';
import PositionAndTime from './PositionAndTime'

export default function NotiDaily(props) {
    const {option} = props
    return (
        <View style={styles.container}>
            <View style={styles.notiDailyLayout}>
                <View style={styles.notiDaily}>
                    <View style={styles.layout}>
                        <View style={styles.notiTitle}>
                            <Text style={styles.title}>Daily Meeting - Billing Team</Text>
                        </View>
                        <View style={styles.place}>
                            <PositionAndTime
                                position={'map-marker-alt'}
                                positionName={'Phòng họp A'}
                                marginLeft={11}
                                fontPosition={10}
                            />
                            <PositionAndTime
                                position={'clock'}
                                positionName={'từ 09h00 đến 10h:00 '}
                                fontPosition={10}
                            />
                        </View>
                    </View>
                    <View style={styles.option}>                        
                        <TouchableOpacity>
                            <Icon name={option?option:''} size={20} />
                        </TouchableOpacity></View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: width,
        height: height / 11,
        justifyContent: 'center',
        alignItems: 'center'
    },
    notiDailyLayout: {
        width: width - 12,
        height: height / 11,
        justifyContent: 'center',
        backgroundColor:COLOR.WHITE,
        borderBottomColor:COLOR.BORDER,
        borderBottomWidth:1
    },
    notiDaily: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    layout:{
        justifyContent:'space-between'
    },
    notiTitle:{
        paddingTop:10,
        paddingHorizontal:7
    },
    title:{
        fontFamily:'Roboto',
        fontStyle:'normal',
        fontWeight:'normal',
        fontSize:14,
        lineHeight:16,
        color:COLOR.PRIMARY_FONT_COLOR
    },
    place:{
        flexDirection:"row",
    },
    option:{
        justifyContent:'center' ,
        paddingHorizontal:8
    }
})