import React, { Component, useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
} from 'react-native'
const { width, height } = Dimensions.get('window')
import { getString } from '../constant/String'
import { COLOR } from '../constant/Colors'
import Icon from 'react-native-vector-icons/FontAwesome5';
import PositionAndTime from './PositionAndTime'

export default function NotificationList(props) {
    const { from, to, name, position, time, marginBottom, fontSize } = props
    return (
        <View style={{alignItems:'center'}}>
             <View style={[styles.notification, {marginBottom:marginBottom}]}>
                <View style={styles.timeMetting}>
                    <View style={styles.chia1}>
                        <Text style={styles.from}>{getString('FROM')}</Text>
                        <Text style={styles.from}>{getString('TO')}</Text>
                    </View>
                    <View style={styles.chia2}>
                        <Text style={styles.to}>{from}</Text>
                        <Text style={styles.to}>{to}</Text>
                    </View>
                    <View style={styles.ngan}>
                    </View>
                </View>
                <View style={styles.notificationDetail}>
                    <View style={styles.option}>
                        <TouchableOpacity >
                            <Icon name='ellipsis-v' size={20} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.title}>
                        <Text style={[styles.textTitle, { fontSize: fontSize }]}>{name}</Text>
                    </View>
                    <View style={styles.place}>
                        <PositionAndTime
                            position={'map-marker-alt'}
                            positionName={position}
                            fontPosition={14}
                           
                        />
                        <PositionAndTime
                            position={'clock'}
                            positionName={time}
                            marginLeft={26}
                            fontPosition={14}
                        />
                    </View>
                </View>
             </View>
        </View>
       
            
      
    )
}
const styles = StyleSheet.create({
    notification: {
        width: width - 12,
        height: height / 6,
        backgroundColor: COLOR.WHITE,
        flexDirection: 'row',
        borderBottomColor: COLOR.BORDER,
        borderBottomWidth:1,
    },
    timeMetting: {
        width: width / 5,
        height: height / 6,
        flexDirection: "row",
    },
    chia1: {
        alignItems: 'flex-end',
        justifyContent: "center",
        paddingLeft: 7,
    },
    from: {
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 10,
        lineHeight: 18,
        color: COLOR.TEXT_INPUT
    },
    chia2: {
        justifyContent: "center",
        marginLeft: 5
    },
    to: {
        fontFamily: 'Sanchez',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 15,
        lineHeight: 19,
        color: COLOR.TEXT_GRAY
    },
    ngan: {
        borderRightColor: COLOR.BORDER_RIGHT,
        borderRightWidth: 1,
        marginLeft: 7,
        marginTop: 18,
        marginBottom: 28
    },
    notificationDetail: {
        flex: 1,
        justifyContent: 'center',
    },
    option: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'flex-end',
        marginRight: 10,
    },
    title: {
        flex: 3,
        justifyContent: 'center',
    },
    textTitle: {
        fontFamily: 'Sanchez',
        fontStyle: 'normal',
        fontWeight: 'normal',
        // fontSize: 18,
        lineHeight: 23,
        color: COLOR.PRIMARY_FONT_COLOR
    },
    place: {
        flex: 2,
        alignItems: 'flex-end',
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
})