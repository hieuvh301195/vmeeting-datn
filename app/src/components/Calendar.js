import React, { Component, useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    StatusBar
} from 'react-native'
const { width, height } = Dimensions.get('window')
import CalendarPicker from 'react-native-calendar-picker';
import { getString } from '../constant/String'
import { COLOR } from '../constant/Colors'

export default function Calendar(props) {
    const [selectedStartDate, setSelectedStartDate] = useState(null)
    function _onDateChange(date){
        setSelectedStartDate(date)
    }
    return (
        <View style={styles.container}>
            <View style={styles.calender}>
                <CalendarPicker
                    width={width}
                    height={height}
                    onDateChange={_onDateChange}
                    selectedDayColor={COLOR.PRIMARY_FONT_COLOR}
                />
            </View>
            <View style={styles.selectedDay}>
                <Text style={styles.textDay}>{selectedStartDate?selectedStartDate.toString():''}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    calender:{
        justifyContent:'center',
        alignItems:'center',
    },
    selectedDay:{
        width:width,
        height:height/12,
        backgroundColor:COLOR.SELECTED_DAY,
        marginTop:-22,
        justifyContent:'center',
        alignItems:'center'
    },
    textDay:{
        fontFamily:'Sanchez',
        fontStyle:'normal',
        fontWeight:'normal',
        fontSize:13,
        lineHeight:17,
        color:COLOR.TEXT_GRAY
    }
})