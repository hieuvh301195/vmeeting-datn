import React, { Component, useState } from 'react'
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    StatusBar
} from 'react-native'
import { COLOR } from '../constant/Colors'
import Icon from 'react-native-vector-icons/FontAwesome'
const { width, height } = Dimensions.get('window')
export default function Header(props) {
    const titleHeader = props.titleHeader
    const name = props.name
    const {navigation} = props
    return (
        <View style={styles.container}>
            <View style={styles.turnBack}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Icon name={name?name:''} size={20} color={'gray'} style={styles.icon} />
                </TouchableOpacity>
            </View>
            <View style={styles._titleHeader}>
                 <Text style={styles.title}>{titleHeader ? titleHeader : 'TitleHeader'}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        width: width,
        height: height / 14,
        flexDirection:"row",
        alignItems:'center',
        backgroundColor:COLOR.BACKGROUND,
        marginBottom:10
    },
    icon:{
        marginLeft:15
    },
    turnBack:{
        height:height/14,
        justifyContent:"center",
    },
    _titleHeader:{
        paddingHorizontal:5,
    },  
    title:{
        fontSize:16,
        lineHeight:18,
        fontFamily:'Sanchez',
        fontStyle:'normal',
        fontWeight:'normal',
        color:COLOR.TEXT_GRAY,
        textTransform:'uppercase'
    }
})