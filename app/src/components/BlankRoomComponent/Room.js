import React, { Component, useState, useEffect } from 'react'
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
} from 'react-native'
import { COLOR } from '../../constant/Colors';
const {width, height} = Dimensions.get('window')
export const roomDefines = [
    { type: 1, name: 'Phòng học', color: COLOR.BIG_ROOM, value:1 },
    { type: 2, name: 'Phòng khách', color: COLOR.SMALL_ROOM, value:2 },
    { type: 3, name: 'Phòng chủ', color: COLOR.CALENDER, value:3 },
    { type: 4, name: 'Phòng trung', color: COLOR.CALENDER, value:4 },
    { type: 5, name: 'Phòng sáng', color: COLOR.CALENDER, value:5},
]
export default function RoomComponent(props) {
    roomDefines
    const {type}  = props;
    const [room, setRoom] = useState(roomDefines[0])
    
    useEffect(()=>{
        _findRoom();
    })

    const _findRoom = () => {
        // for (let i = 0; i < roomDefines.length; i++) {
        //     if (roomDefines[i].type === type){
        //         setRoom(roomDefines[i])
        //     }
        // }
        roomDefines.map((item,index) =>{
            if(item.type === type){
                setRoom(item);
            }
        })
    };

    return (
        <View style={[styles.container, { backgroundColor: room.color }]}>
            <Text style={styles.text}>{room.name}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: 8,
        paddingHorizontal: 10,
        borderRadius: 999,
        backgroundColor: COLOR.PRIMARY_FONT_COLOR,
        marginLeft:5,
        justifyContent:'center',
        alignItems:'center',
        marginBottom:5
    },
    text: {
        color: COLOR.WHITE,
        fontSize:13,
        fontFamily:'Sanchez',
        fontStyle:'normal',
        fontWeight:'normal'
    }
})