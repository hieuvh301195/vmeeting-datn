import React, { Component, useState, useEffect } from 'react'
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
} from 'react-native'
import { COLOR } from '../../constant/Colors'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Room from './Room'
import { BOOK } from '../../navigations/NavigationName'

const myListRoom = [1,2,3]

const { width, height } = Dimensions.get('window')
export default function ListRoom(props) {
    const {time, room} = props
    const nativeToBook= props.nativeToBook
    return (
        <View style={styles.container}>
            <View style={styles.itemList}>
                <View style={styles.time}>
                    <Text style={styles.textTime}>{time}</Text>
                </View>
                <View style={styles.roomCard}>
                    {room.map((item, index) => {
                        return <Room type={item} />
                    })}
                    <TouchableOpacity style={styles.buttonAdd} onPress={nativeToBook}>
                        <Text style={styles.textPlus}>+</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: width,
        alignItems: 'center',
        justifyContent:'center',
    },
    itemList: {
        width: '90%',
        flexDirection: "row",
        borderBottomWidth: COLOR.BORDER_WIDTH,
        borderBottomColor: COLOR.TEXT_GRAY,
    },
    time: {
        justifyContent: 'center',
        paddingVertical: 15,
        width: width / 5,
        borderRightColor: COLOR.TEXT_GRAY,
        borderRightWidth: COLOR.BORDER_WIDTH,
    },
    textTime:{
        fontFamily:'Sanchez',
        fontStyle:'normal',
        fontWeight:'normal',
        fontSize:12,
        lineHeight:15,
        color:COLOR.TEXT_GRAY
    },
    roomCard:{
        flex:1,
        flexDirection:'row',
        flexWrap:'wrap',
        paddingVertical:18,
        
    },
    buttonAdd:{
        paddingHorizontal:18,
        paddingVertical:8,
        backgroundColor:COLOR.PRIMARY_COLOR,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:10,
        marginLeft:9,
    },
    textPlus:{
        color:COLOR.WHITE,
        fontSize:14,
        fontFamily:'Sanchez',
        fontStyle:'normal',
        fontWeight:'bold',
    }
})