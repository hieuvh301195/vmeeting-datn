import React, { Component, useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    StatusBar,
    FlatList,
    TouchableOpacity
} from 'react-native'
const { width, height } = Dimensions.get('window')
import { getString } from '../constant/String'
import { COLOR } from '../constant/Colors'

export default function showMoreButton(props){
    return(
        <View style={styles.container}>
            <TouchableOpacity style={styles.showMore}>
                <Text style={styles.textShowMore}>Xem thêm</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingVertical:14,
        alignItems:'center',
    },
    showMore:{
        backgroundColor:COLOR.SHOW_MORE,
        paddingHorizontal:28,
        paddingVertical:6,
        borderRadius:4,
        justifyContent:'center',
        alignItems:'center'
    },
    textShowMore:{
        fontFamily:'RV Harmonia',
        fontSize:10,
        lineHeight:13,
        color:COLOR.PRIMARY_FONT_COLOR
    }
})