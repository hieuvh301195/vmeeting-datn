import React, { useState, useEffect } from 'react'
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    TextInput,
    Picker
} from 'react-native'

import { COLOR } from '../../constant/Colors'
const { width, height } = Dimensions.get('window')

// const roomData=[
//     { type: 1, name: 'Phòng học', color: COLOR.BIG_ROOM },
//     { type: 2, name: 'Phòng khách', color: COLOR.SMALL_ROOM },
//     { type: 3, name: 'Phòng chủ', color: COLOR.CALENDER },
//     { type: 4, name: 'Phòng trung', color: COLOR.CALENDER },
//     { type: 5, name: 'Phòng sáng', color: COLOR.CALENDER },
// ]
export default function BookItem(props) {
    const { title } = props
    return (
        <View style={styles.container}>
            <View style={styles.bookInput}>
                <Text style={styles.titleInput}>{title}</Text>
                <View style={styles.isTyping}>
                    <TextInput 
                        style={styles.textInput}
                    />
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 10
    },
    bookInput: {
        width: '90%',
        justifyContent: 'center',
    },
    isTyping: {
        width: '90%',
        borderBottomColor: COLOR.TEXT_INPUT,
        borderBottomWidth: 1,
    },
    textInput: {
        marginVertical: -9,
        marginLeft: -4,
        color: COLOR.TEXT_INPUT,
        fontSize: 12,
        fontFamily: 'Sanchez',
        fontStyle: 'normal',
        fontWeight: "normal",
        lineHeight: 13
    },
    titleInput: {
        fontFamily: 'Sanchez',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 14,
        lineHeight: 15,
        color: COLOR.TEXT_LABEL
    }
})

