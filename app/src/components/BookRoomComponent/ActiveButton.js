import React, { Component, useState, useEffect } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    TouchableOpacity
} from 'react-native'
import { COLOR } from '../../constant/Colors';
const { width, height } = Dimensions.get('window');

export default function ActiveButton(props) {
    const {activeCancel,textActive,backgroundcolor} = props;
    const active = backgroundcolor?backgroundcolor:COLOR.GRAY
    return (
        <View>
            <TouchableOpacity style={[styles.cancel, {backgroundColor:active}]} onPress={activeCancel}>
                <Text style={styles.textActive}>{textActive?textActive:'textActive'}</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    cancel:{
        width:width/4,
        paddingVertical:6,
        borderRadius:5,
        marginHorizontal:6,
        alignItems:'center'
    },
    apply:{
        width:width/4,
        paddingVertical:6,
        backgroundColor:COLOR.GREEN,
        borderRadius:5,
        marginHorizontal:6,
        marginVertical:5,
        alignItems:'center'
    },
    textActive:{
        color: COLOR.WHITE,
        fontSize:13,
        fontFamily:'Sanchez',
        fontStyle:'normal',
        fontWeight:'normal'
    }
})