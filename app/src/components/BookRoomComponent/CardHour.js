import React, { Component, useState, useEffect } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    TouchableOpacity
} from 'react-native'
import { COLOR } from '../../constant/Colors';
const { width, height } = Dimensions.get('window');
const dataTime = [
    { id: 1, time: '9h30-10h00', backgroundColor: COLOR.FACEBOOK },
    { id: 2, time: '9h30-10h00', backgroundColor: COLOR.FACEBOOK },
    { id: 3, time: '9h30-10h00', backgroundColor: COLOR.GRAY },
    { id: 4, time: '9h30-10h00', backgroundColor: COLOR.GRAY },
    { id: 5, time: '9h30-10h00', backgroundColor: COLOR.GRAY },
    { id: 6, time: '9h30-10h00', backgroundColor: COLOR.GRAY },
    { id: 7, time: '9h30-10h00', backgroundColor: COLOR.GRAY },
    { id: 8, time: '9h30-10h00', backgroundColor: COLOR.GRAY },
    { id: 9, time: '9h30-10h00', backgroundColor: COLOR.FACEBOOK },
    { id: 10, time: '9h30-10h00', backgroundColor: COLOR.GRAY },
    { id: 11, time: '9h30-10h00', backgroundColor: COLOR.CAUTION },
    { id: 12, time: '9h30-10h00', backgroundColor: COLOR.FACEBOOK },
    { id: 13, time: '9h30-10h00', backgroundColor: COLOR.CAUTION },
    { id: 14, time: '9h30-10h00', backgroundColor: COLOR.CAUTION },
    { id: 15, time: '9h30-10h00', backgroundColor: COLOR.CAUTION },
]

export default function CardHour() {
    const [time, setTime] = useState(dataTime[0])
    // useEffect(() =>{
    //     _fineTime()
    // })

    // const _fineTime = () =>{
    //     dataTime.map((item,index) =>{
    //         if(item.id === id){
    //             setTime(item)
    //         }
    //     })
    // }
    return (
        <View style={styles.container}>
            {dataTime.map((item, index) => {
                return (
                    <TouchableOpacity>
                        <View style={[styles.cardHour, { backgroundColor: item.backgroundColor }]}>
                            <Text style={styles.textTime}>{item.time}</Text>
                        </View>
                    </TouchableOpacity>

                )
            })}
        </View>

    )
}

const styles = StyleSheet.create({
    container: {
        width: '90%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginBottom: 29
    },
    // cardHour: {
    //     flexDirection: 'row',
    //     flexWrap: 'wrap',
    //     width: '90%',
    //     marginBottom: 29
    // },
    cardHour: {
        width: width / 4,
        paddingVertical: 7,
        backgroundColor: COLOR.FACEBOOK,
        borderRadius: 5,
        marginHorizontal: 4,
        marginVertical: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textTime: {
        fontFamily: 'Sanchez',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 12,
        color: COLOR.WHITE,
        lineHeight: 13
    }
})