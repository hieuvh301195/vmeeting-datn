import React, { Component, useState, useEffect } from 'react'
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
} from 'react-native'
import { COLOR } from '../../constant/Colors';
const {width, height} = Dimensions.get('window')
const cardMember = [
    { id: 1, name: 'Anh Tú', color: COLOR.PRIMARY_COLOR },
    { id: 2, name: 'Huy Hiếu', color: COLOR.PRIMARY_COLOR },
    { id: 3, name: 'Huy Hùng', color: COLOR.PRIMARY_COLOR },
    { id: 4, name: 'Hữu Tuấn', color: COLOR.PRIMARY_COLOR },
]

export default function CardMember(props) {
    const {id}  = props;
    const [member, setMember] = useState(cardMember[0])

    useEffect(()=>{
        _findMember();
    })

    const _findMember = () => {
        cardMember.map((item,index) =>{
            if(item.id === id){
                setMember(item);
            }
        })
    };

    return (
        <View style={{}}>
             <View style={[styles.container, { backgroundColor: member.color }]}>
            <Text style={styles.text}>{member.name}</Text>
        </View>
        </View>
       
    )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 16,
        marginTop:10,
        borderRadius: 999,
        backgroundColor: COLOR.PRIMARY_FONT_COLOR,
        marginRight:9,
        justifyContent:'center',
        alignItems:'center',
    },
    text: {
        color: COLOR.WHITE,
        fontSize:13,
        fontFamily:'Sanchez',
        fontStyle:'normal',
        fontWeight:'normal'
    }
})