import React, { useState } from 'react'
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    TextInput,
    TouchableOpacity
} from 'react-native'
import { COLOR } from '../../constant/Colors'
import CardMember from './CardMember'
const { width, height } = Dimensions.get('window')
const name=[1,2,3,4]
export default function AddMember(props) {
    return (
        <View style={styles.container}>
            <View style={styles.bookInput}>
                <Text style={styles.titleInput}>Người tham gia</Text>
                <View style={styles.memberCard}>
                    {name.map((item,index)=>{
                        return <CardMember id={item}/>
                    })}
                    <TouchableOpacity style={styles.buttonAdd}>
                        <Text style={ styles.textPlus}>Thêm   +</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 10
    },
    bookInput: {
        width: '90%',
        justifyContent: 'center',
    },
    memberCard: {
        width: '90%',
        borderBottomColor: COLOR.TEXT_INPUT,
        borderBottomWidth: 1,
        flexDirection:'row',
        flexWrap:'wrap',
    },
    buttonAdd:{
        paddingHorizontal:20,
        marginVertical:9,
        backgroundColor:COLOR.FACEBOOK,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:999,
        
    },
    titleInput: {
        fontFamily: 'Sanchez',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 14,
        lineHeight: 15,
        color: COLOR.TEXT_LABEL
    },
    textPlus:{
        color:COLOR.WHITE,
        fontSize:14,
        fontFamily:'Sanchez',
        fontStyle:'normal',
        fontWeight:'normal',
    }
})
