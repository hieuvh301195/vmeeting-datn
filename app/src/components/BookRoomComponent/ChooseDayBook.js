import React, { Component, useState, useEffect } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    TouchableOpacity
} from 'react-native'
import { COLOR } from '../../constant/Colors';
import CardHour from './CardHour'
import ActiveButton from './ActiveButton'
const { width, height } = Dimensions.get('window');
const datadays = [
    { id: 1, name: 'T2' },
    { id: 2, name: 'T3' },
    { id: 3, name: 'T4' },
    { id: 4, name: 'T5' },
    { id: 5, name: 'T6' },
    { id: 6, name: 'T7' },
]

export default function ChooseDay(props) {
    const { isChooseDay, cancel } = props;
    if (isChooseDay) {
        return (
            <View style={styles.background}>
                <View style={styles.pickRoom}>
                    <View style={styles.container}>
                        <Text>Chọn ngày</Text>
                    </View>
                    <View style={styles.days}>
                        {datadays.map((item, index) => {
                            return (
                                <TouchableOpacity>
                                    <View style={styles.day}>
                                        <Text>{item.name}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        })}
                    </View>
                    <CardHour />
                    <View style={styles.pickButton}>
                        <ActiveButton
                            activeCancel={cancel}
                            textActive={'Cancel'}
                            backgroundcolor={COLOR.CAUTION}
                        />
                        <ActiveButton
                            textActive={'Apply'}
                            backgroundcolor={COLOR.GREEN}
                        />
                    </View>
                </View>
            </View>
        )
    }
    return <View />
}
const styles = StyleSheet.create({
    background: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        backgroundColor: "#00000099",
        width,
        height,
        position: 'absolute',
        bottom: 0,
        elevation: 8

    },
    pickRoom: {
        width: '90%',
        backgroundColor: 'white',
        borderRadius: 10,
        marginVertical: 11,
        alignItems: 'center'
    },
    container: {
        width: '90%',
        height: height / 14,

        justifyContent: 'center',
    },
    days: {
        flexDirection: 'row',
        width: '90%',
        justifyContent: 'center',
        marginBottom: 10
    },
    day: {
        paddingHorizontal: 12,
        paddingVertical: 10,
        borderRadius: 999,
        backgroundColor: 'silver',
        marginHorizontal: 5
    },
    text: {
        marginLeft: 15
    },
    pickButton:{
        flexDirection: 'row',
        width: '90%',
        justifyContent:'center',
        alignItems:'center',
        marginVertical:11,
    }
})
