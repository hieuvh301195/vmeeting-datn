import React, { useState, useEffect } from 'react'
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    TextInput,
    Picker
} from 'react-native'

import { COLOR } from '../../constant/Colors'
import { getString } from '../../constant/String'
const { width, height } = Dimensions.get('window')
import {roomDefines} from '../BlankRoomComponent/Room'
const dataRoom = [roomDefines][0]
export default function BookItem(props) {
    const [selectedValue, setSelectedValue] = useState()
    return (
        <View style={styles.container}>
            <View style={styles.bookInput}>
                <Text style={styles.titleInput}>{getString('MEETING_ROOM')}</Text>
                <View style={styles.isTyping}>
                    <Picker
                        selectedValue={selectedValue}
                        style={styles.textInput}
                        onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                        mode='dropdown'
                    >
                        {dataRoom.map((item) => {
                            return(
                                <Picker.Item 
                                    label={item.name} 
                                    value={item.value}                                    
                                />
                            )
                        })}
                    </Picker>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 10
    },
    bookInput: {
        width: '90%',
        justifyContent: 'center',
    },
    isTyping: {
        width: '90%',
        borderBottomColor: COLOR.TEXT_INPUT,
        borderBottomWidth: 1,
    },
    textInput: {
        marginVertical: -9,
        marginLeft: -8,
        color: COLOR.TEXT_INPUT,
        fontSize: 12,
        fontFamily: 'Sanchez',
        fontStyle: 'normal',
        fontWeight: "normal",
        lineHeight: 13
    },
    titleInput: {
        fontFamily: 'Sanchez',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 14,
        lineHeight: 15,
        color: COLOR.TEXT_LABEL
    },
    fake:{
        height:height/10
    }
})

