import React, { Component, useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
} from 'react-native'
import { COLOR } from '../constant/Colors'
import Icon from 'react-native-vector-icons/FontAwesome5';

export default function PositionAndTime(props) {
    const { position, positionName, marginLeft, fontPosition } = props
    return (
        <View style={[styles.position, {marginLeft:marginLeft}]}>
            <Icon name={position ? position : 'map-marker-alt'} color={COLOR.TEXT_INPUT} size={15} />
            <Text style={[styles.positionName, {fontSize:fontPosition}]}>{positionName ? positionName : 'Phòng bé'}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    position: {
        flexDirection: 'row',
        marginBottom: 7,
        marginLeft:26,
        marginRight: 10
    },
    positionName: {
        fontFamily: 'Sanchez',
        fontStyle: 'normal',
        fontWeight: 'normal',
        lineHeight: 14,
        color: COLOR.TEXT_GRAY,
        marginLeft: 4,
        marginTop: 3
    }
})