import React, { Component, useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    Image
} from 'react-native'
import { COLOR } from '../constant/Colors'
const { width, height } = Dimensions.get('window')
export default function AccountInfo(props) {
    const { info, infoDetail } = props;
    return (
        <View style={styles.container}>
            <View style={styles.info}>
                <Text style={styles.textInfo}>{info ? info : 'info'}</Text>
            </View>
            <View style={styles.infoDetail}>
                <Text style={styles.textDetail}>{infoDetail ? infoDetail : 'infoDetail'}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: width - 14,
        height: height / 10,
        backgroundColor: COLOR.WHITE,
        marginBottom: 17,
        borderBottomColor:'rgba(0, 0, 0, 0.1);',
        borderBottomWidth:1,
        borderRadius:5
    },
    info: {
        flex: 3,
        justifyContent: 'center',
        paddingHorizontal: 15,
    },
    textInfo: {
        fontFamily: 'Sanchez',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 10,
        lineHeight: 13,
        color: COLOR.TEXT_INFO
    },
    infoDetail: {
        flex: 3,
        paddingHorizontal: 15,
        marginTop: -5
    },
    textDetail: {
        fontFamily: 'Sanchez',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 15,
        lineHeight: 13,
        paddingVertical: 5,
        color: COLOR.TEXT_TIME
    }
})