const vn = {
    'LOGIN': 'Đăng nhập',
    'LIST_ROOM':'Danh sách phòng họp',
    'BOOK_ROOM':'Đặt lịch họp',
    'NOTIFICATION':'Thông báo trong ngày',
    'FROM':'từ',
    'TO':'đến',
    'MEETING_ROOM':'Phòng họp',
    'ON-GOING':'Lịch họp sắp tới',
}

export const getString = (code) => {
    return vn[code]
}

