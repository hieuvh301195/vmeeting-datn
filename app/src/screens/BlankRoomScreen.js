import React, { Component, useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    FlatList,
    StatusBar,
    ScrollView
} from 'react-native'
const { width, height } = Dimensions.get('window')
import Header from '../components/Header'
import { getString } from '../constant/String'
import ListRoom from '../components/BlankRoomComponent/ListRoom'
import { COLOR } from '../constant/Colors'
import { BOOK } from '../navigations/NavigationName'
const dataListRoom = [
    { id: 1, time: '09h:00', room: [] },
    { id: 2, time: '09h:30', room: [1, 2] },
    { id: 3, time: '10h:00', room: [1, 2] },
    { id: 4, time: '10h:30', room: [1, 2, 3] },
    { id: 5, time: '11h:00', room: [1,2] },
    { id: 6, time: '11h:30', room: [1] },
    { id: 7, time: '12h:00', room: [2] },
    { id: 8, time: '12h:30', room: [1,2] },
    { id: 9, time: '13h:00', room: [] },
    { id: 10, time: '13h:30', room: [] },
    { id: 11, time: '14h:00', room: [1,2] },
]
export default function BlankRoomScreen(props) {
    function _nativeToBook() {
        const { navigation } = props;
        navigation.navigate(BOOK);
    }
    return (
        <View style={styles.container}>
            <Header
                titleHeader={getString('LIST_ROOM')}
            />
            <FlatList
                data={dataListRoom}
                renderItem={({ item }) => (
                    <ListRoom
                        nativeToBook={() => _nativeToBook()}
                        id={item.id}
                        time={item.time}
                        room={item.room}
                    />
                )}
                keyExtractor={item => item.id}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.BACKGROUND,
        paddingVertical: StatusBar.currentHeight
    },
})