import React, { Component, useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    StatusBar,
    ScrollView
} from 'react-native'
import { COLOR } from '../constant/Colors'
import Calendar from '../components/Calendar'
const { width, height } = Dimensions.get('window')
import { dataNotification } from './NotificationScreen'
import NotiDaily from '../components/NotiDaily'
const selectedDayData = [dataNotification][0]
export default function MeetingSchedule(props) {
    return (
        <ScrollView>
            <View style={styles.container}>
                <Calendar />
                {selectedDayData.slice(0,4).map((item, index) => {
                    return (
                        <NotiDaily
                            id={item.id}
                            name={item.name}
                            position={item.position}
                            time={item.time}
                            fontSize={14}
                        />
                    )
                })}
            </View>
        </ScrollView>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.BACKGROUND,
        paddingVertical: StatusBar.currentHeight,
        alignItems: 'center'
    },
})