import React, { Component, useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    StatusBar,
    FlatList,
    ScrollView,
    SafeAreaView
} from 'react-native'
const { width, height } = Dimensions.get('window')
import { getString } from '../constant/String'
import { COLOR } from '../constant/Colors'
import Header from '../components/Header'
import NotificationList from '../components/NotificationList'
import { dataNotification } from './NotificationScreen'
import ShowMoreButton from '../components/ShowMoreButton'
import NotiDaily from '../components/NotiDaily'
const onGoingData = [dataNotification][0]
export default function BlankRoomScreen(props) {
    return (
        <SafeAreaView>
            <ScrollView>
                <View style={styles.container}>
                    <Header
                        titleHeader={getString('ON-GOING')}
                    />
                    {onGoingData.slice(0, 2).map((item, index) => {
                        return (
                            <NotificationList
                                id={item.id}
                                from={item.from}
                                to={item.to}
                                name={item.name}
                                position={item.position}
                                time={item.time}
                                fontSize={14}
                            />
                        )
                    })}
                    <ShowMoreButton />
                    <Header
                        titleHeader={'Thứ 3, 02/07'}
                    />
                    {onGoingData.slice(0, 4).map((item, index) => {
                        return (
                            <NotiDaily
                                id={item.id}
                                name={item.name}
                                position={item.position}
                                time={item.time}
                                fontSize={14}
                                option={'ellipsis-v'}
                            />
                        )
                    })}
                    <ShowMoreButton />
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: COLOR.BACKGROUND,
        paddingVertical: StatusBar.currentHeight,
    },
})