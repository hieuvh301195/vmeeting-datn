import React, { Component, useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    StatusBar,
    FlatList
} from 'react-native'
const { width, height } = Dimensions.get('window')
import Header from '../components/Header'
import { getString } from '../constant/String'
import { COLOR } from '../constant/Colors'
import NotificationList from '../components/NotificationList'
import Icon from 'react-native-vector-icons/FontAwesome5';
export const dataNotification = [
    { id: 1, from: '10:00', to: '11:00', name: 'Daily Meeting', position: 'Phòng bé', time: '60 phút' },
    { id: 2, from: '13:00', to: '13:30', name: 'Họp con Server Management', position: 'Phòng bé', time: '30 phút' },
    { id: 3, from: '16:00', to: '17:00', name: 'Sinh nhật trong tháng', position: 'Phòng mrk', time: '60 phút' },
    { id: 4, from: '17:00', to: '17:30', name: 'Họp app VMeeting', position: 'Phòng họp A', time: '30 phút' },
]
export default function BlankRoomScreen(props) {
    dataNotification;
    return (
        <View style={styles.container}>
            <Header
                titleHeader={getString('NOTIFICATION')}
            />
            <FlatList
                data={dataNotification}
                renderItem={({ item }) => (
                    <NotificationList
                        id={item.id}
                        from={item.from}
                        to={item.to}
                        name={item.name}
                        position={item.position}
                        time={item.time}
                        marginBottom={16}
                        fontSize={18}
                    />
                )}
                keyExtractor={item => item.id}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.BACKGROUND,
        paddingVertical: StatusBar.currentHeight,
        alignItems: 'center'
    },
})