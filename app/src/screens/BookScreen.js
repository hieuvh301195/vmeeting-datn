import React, { Component, useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    StatusBar,
    FlatList,
    TouchableOpacity
} from 'react-native'
const { width, height } = Dimensions.get('window')
import Header from '../components/Header'
import { getString } from '../constant/String'
import { COLOR } from '../constant/Colors'
import BookItem from '../components/BookRoomComponent/BookItemList'
import AddMember from '../components/BookRoomComponent/AddMember'
import RoomToBook from '../components/BookRoomComponent/RoomToBook'
// const typing=[1,2]
const textTitle = [
    { id: 1, name: 'Thời gian họp' },
    { id: 2, name: 'Nội dung họp' },
]
import ChooseDayBook from '../components/BookRoomComponent/ChooseDayBook'
export default function BlankRoomScreen(props) {
    const [chooseDay, setChooseDay] = useState(false)
    function _chooseDay(){
        setChooseDay(true)
    }
    function _cancelChooseDay(){
        setChooseDay(false)
    }
    return (
        
        <View style={styles.container}>
            <Header
                name={'chevron-left'}
                titleHeader={getString('BOOK_ROOM')}
                {...props}
            />
            <RoomToBook/>
            {textTitle.map((item,index)=>{
                return <BookItem title={item.name}/>
            })}
            <AddMember/>
            <TouchableOpacity style={styles.bookButton} onPress={() => _chooseDay()}>
                <Text style={styles.bookNow}>Đặt ngay</Text>
            </TouchableOpacity>
            <ChooseDayBook
                isChooseDay={chooseDay}
                cancel={() => _cancelChooseDay()}
            />
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.BACKGROUND,
        paddingVertical: StatusBar.currentHeight,
        alignItems: 'center'
    },
    bookButton:{
        width:width/2,
        height:height/14,
        backgroundColor:COLOR.FACEBOOK,
        borderRadius:10,
        justifyContent:'center',
        alignItems:'center',
        marginVertical:19,
    },
    bookNow:{
        fontFamily:'Sanchez',
        fontStyle:'normal',
        fontWeight:'normal',
        fontSize:15,
        lineHeight:19,
        color:COLOR.WHITE
    }

})