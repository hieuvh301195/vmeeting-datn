import React, { Component, useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    ImageBackground,
    ScrollView
} from 'react-native'
const { width, height } = Dimensions.get('window')
import { BOTTOM_TAB_BAR } from '../navigations/NavigationName'
import Loading from '../components/Loading'
import { connect } from 'react-redux'
import {
    GoogleSignin,
    GoogleSigninButton,
    statusCodes,
} from '@react-native-community/google-signin';

class LoginScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            userInfo:null
        }
    }
    componentDidMount(){
        GoogleSignin.configure({
            scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
            webClientId:'335540619842-qttadmphnc64odgdfh1ctm39bi4r086s.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
            offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
            //hostedDomain: '', // specifies a hosted domain restriction
            //loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
            forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
            //accountName: '', // [Android] specifies an account name on the device that should be used
            //iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
          });
    }
    // _nativeTo() {
    //     const { navigation } = this.props;
    //     this.props.dispatch({
    //         type: 'LOADING'
    //     })
    //     setTimeout(function () {
    //         // this.props.dispatch({
    //         //     type:'LOADING'
    //         // })
    //         // setTimeout(function(){
    //         navigation.navigate(BOTTOM_TAB_BAR)
    //     }, 3000);
    // }
    _signIn = async () => {
        try {
          await GoogleSignin.hasPlayServices();
          const userInfo = await GoogleSignin.signIn();
          console.log('user info: ',userInfo)
          this.setState({ userInfo });
        } catch (error) {
          if (error.code === statusCodes.SIGN_IN_CANCELLED) {
            // user cancelled the login flow
          } else if (error.code === statusCodes.IN_PROGRESS) {
            // operation (e.g. sign in) is in progress already
          } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
            // play services not available or outdated
          } else {
            // some other error happened
          }
          console.log('error: ', error)
        }
      };
    render() {
        console.log('userInfo', this.state.userInfo)
        return (
            <View style={styles.container}>
                <ImageBackground
                    source={require('../Image/background.png')}
                    style={{ width, height, }}>
                    {/* <View style={{ paddingVertical: 440, paddingHorizontal: 36 }}>
                        <Text style={{ fontSize: 45, color: 'white', fontWeight: 'bold' }}>nMeeting</Text>
                        <Text style={{ borderBottomColor: '#979797', borderBottomWidth: 1, width: 75, marginTop: -25 }}></Text>
                        <Text style={{ paddingVertical: 14 }}>built by NextTer for NextTer</Text>
                        <View style={{ paddingVertical: 30 }}> */}
                            {/* <TouchableOpacity style={styles.buttonLogin} onPress={() => this._nativeTo()}>
                                <Text>Đăng nhập với Email NextTech</Text>
                            </TouchableOpacity>
                            {this.props.myLoading ? <Loading /> : null} */}
                            <GoogleSigninButton
                                style={{ width: 192, height: 48 }}
                                size={GoogleSigninButton.Size.Wide}
                                color={GoogleSigninButton.Color.Dark}
                                onPress={this._signIn}
                                //disabled={this.state.isSigninInProgress} 
                            />
                        {/* </View>
                    </View> */}
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',

    },
    buttonLogin: {
        width: height / 2,
        height: height / 16,
        backgroundColor: '#FF647C',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 6,
    }
})

function mapStateToProps(state) {
    return {
        myLoading: state.isLoading
    }
}

export default connect(mapStateToProps)(LoginScreen)
