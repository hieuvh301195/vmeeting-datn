import React, { Component, useState, useEffect } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    StatusBar,
    FlatList
} from 'react-native'
const { width, height } = Dimensions.get('window')
import Header from '../components/Header'
import Avatar from '../components/Avatar'
import AccountInfo from '../components/AccountInfo'
const accountData = [
    { id: 1, label: 'Email', value: 'tu240898@gmail.com' },
    { id: 2, label: 'Địa chỉ', value:'Kham Thien, Ha Noi'},
    { id: 3, label: 'Ngày sinh', value: '24-08-1998' },
    { id: 4, label: 'Thời gian nhận thông báo', value: '30 phút'},
]

export default function BlankRoomScreen(props) {
    return (
        <View style={styles.container}>
            <Avatar />
            <FlatList
                data={accountData}
                renderItem={({ item }) => (
                    <AccountInfo
                        id={item.id}
                        info={item.label}
                        infoDetail={item.value}
                    />
                )}
                keyExtractor={item => item.id}
            />
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: StatusBar.currentHeight,
        alignItems: 'center'
    },
})