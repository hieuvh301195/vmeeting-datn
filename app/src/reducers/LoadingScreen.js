const loadingScreen = (isLoading = false, action) =>{
    switch (action.type) {
        case 'LOADING':
            return {
                isLoading:!isLoading,
            }
        default:
          return isLoading
      }
}
export default loadingScreen;
