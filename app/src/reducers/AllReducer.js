import showMore from './ShowMore'
import isLoading from './LoadingScreen'
// import noJoin from './NoJoin'
// import deleteRoom from './DeleteRoom'
// import addRoom from './AddRoom'
// import abort from './Abort'
import { combineReducers } from 'redux';

const allReducers = combineReducers({
    showMore,
    isLoading,
    // noJoin,
    // deleteRoom,
    // addRoom,
    // abort,
})

export default allReducers;
