import React, { Component, useState } from 'react'
import {
    View,
    Text,
} from 'react-native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import BlankRoom from '../screens/BlankRoomScreen'
import MeetingSchedule from '../screens/MeetingScheduleScreen'
import OnGoing from '../screens/OnGoingScreen'
import Notification from '../screens/NotificationScreen'
import Account from '../screens/AccountScreen'
import { BLANK_ROOM, MEETING_SCHEDULE, ON_GOING, NOTIFICATION, ACCOUNT } from './NavigationName'
const Tab = createBottomTabNavigator();
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { COLOR } from '../constant/Colors';

export default class NavigationBottom extends Component {
    render() {
        return (
            <Tab.Navigator
                initialRouteName="BlankRoom"
                tabBarOptions={{
                    activeTintColor: COLOR.PRIMARY_COLOR,
                }}
            >
                <Tab.Screen name={BLANK_ROOM} component={BlankRoom}
                    options={{
                        tabBarLabel: BLANK_ROOM,
                        tabBarIcon: ({ color, size }) => (
                            <MaterialCommunityIcons name="magnify" color={color} size={size} />
                        ),
                    }}
                />
                <Tab.Screen name={MEETING_SCHEDULE} component={MeetingSchedule} 
                     options={{
                        tabBarLabel: MEETING_SCHEDULE,
                        tabBarIcon: ({ color, size }) => (
                            <MaterialCommunityIcons name="calendar" color={color} size={size} />
                        ),
                    }}
                />
                <Tab.Screen name={ON_GOING} component={OnGoing} 
                     options={{
                        tabBarLabel: ON_GOING,
                        tabBarIcon: ({ color, size }) => (
                            <MaterialCommunityIcons name="compass" color={color} size={size} />
                        ),
                    }}
                />
                <Tab.Screen name={NOTIFICATION} component={Notification} 
                     options={{
                        tabBarLabel: NOTIFICATION,
                        tabBarIcon: ({ color, size }) => (
                            <MaterialCommunityIcons name="bell" color={color} size={size} />
                        ),
                    }}
                />
                <Tab.Screen name={ACCOUNT} component={Account} 
                     options={{
                        tabBarLabel: ACCOUNT,
                        tabBarIcon: ({ color, size }) => (
                            <MaterialCommunityIcons name="account-circle" color={color} size={size} />
                        ),
                    }}
                />
            </Tab.Navigator>
        )
    }
}