import React, { Component, useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Dimensions
} from 'react-native'
const { width, height } = Dimensions.get('window')
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../screens/LoginScreen'
import BlankRoomScreen from '../screens/BlankRoomScreen'
import BookScreen from '../screens/BookScreen'
import {LOGIN,BOTTOM_TAB_BAR, BOOK} from './NavigationName'
import BottomNavigation from './NavigationBottomScreen'
export default class Navigation extends Component {
    render() {
        const Stack = createStackNavigator();
        return (
            <NavigationContainer>
                <Stack.Navigator headerMode={'none'}>
                    <Stack.Screen name={LOGIN} component={LoginScreen} />
                    {/* <Stack.Screen name={BOTTOM_TAB_BAR} component={BottomNavigation} />
                    <Stack.Screen name={BOOK} component={BookScreen} /> */}
                </Stack.Navigator>
            </NavigationContainer>
        )
    }
}