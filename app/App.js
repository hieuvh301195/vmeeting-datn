import 'react-native-gesture-handler';
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import allReducers from './src/reducers/AllReducer'
import Navigation from './src/navigations/Navigation'
const App: () => React$Node = () => {
  return (
    <Provider store={store} >
      <StatusBar barStyle="dark-content" backgroundColor="transparent" translucent={true}/>
      <Navigation/>
    </Provider>
  );
};

const store = createStore(allReducers)

const styles = StyleSheet.create({

});

export default App;
